let app = new PIXI.Application(
  {
    transparent: true
  }
);

app.renderer.background.color = 0xadd8e6;
app.renderer.resize(window.innerWidth, window.innerHeight);
document.body.appendChild(app.view);

const boidCount = 200;
const boidScale = 4;
const separationRadius = 40;
const alignmentRadius = 80;
const threatAvoidanceRadius = 100;
const edgeMargin = 240;
const edgeSlope = 0.5;
const edgeAvoidanceFactor = 0.005;
const alignmentFactor = 0.06;
const separationFactor = 0.01;
const cohesionFactor = 0.003;
const threatAvoidanceFactor = 1;
const minSpeed = 2;
const maxSpeed = 6;
// const decelerationFactor = 1;
const randomAccelerationFactor = 0.02;


const zone = new PIXI.Graphics();
// zone.lineStyle(2, 0xaa0000, 1);
// zone.beginFill(0x000000, 0.1);
// zone.drawRect(edgeMargin, edgeMargin, app.renderer.screen.width - 2 * edgeMargin, app.renderer.screen.height - 2 * edgeMargin);
// zone.endFill();
zone.alpha = 0;
app.stage.addChild(zone);

const cursorZone = new PIXI.Graphics();
cursorZone.interactive = true;
cursorZone.beginFill(0x000000, 0.1);
cursorZone.drawRect(0, 0, app.renderer.screen.width, app.renderer.screen.height);
cursorZone.endFill();
cursorZone.alpha = 0;
app.stage.addChild(cursorZone);

const cursor = new PIXI.Graphics();
cursor.beginFill(0x440000, 0.1);
cursor.drawCircle(0, 0, threatAvoidanceRadius);
cursor.endFill();
app.stage.addChild(cursor);

// const cursorText = new PIXI.Text();
// cursor.text = cursorText;
// app.stage.addChild(cursor.text);


const boidTemplate = new PIXI.Graphics();
// boidTemplate.beginFill(0x4169e1);
boidTemplate.beginFill(0x111111);
boidTemplate.drawPolygon([5, 0, -3, -2, -3, 2].map(v => v * boidScale));
boidTemplate.endFill();

// boidTemplate.lineStyle(2, 0x4169e1, 1);
// boidTemplate.drawCircle(0, 0, alignmentRadius);
// boidTemplate.lineStyle(2, 0xaa0000, 1);
// boidTemplate.drawCircle(0, 0, separationRadius);

const boidTexture = app.renderer.generateTexture(boidTemplate);


const doubleBoundary = (x, {breakLeft, breakRight}) => {
  if (x < breakLeft) {
    return edgeSlope;
  };
  if (x > breakRight) {
    return -edgeSlope;
  }
  return 0
}

const doubleRamp = (x, { breakLeft, breakRight, slopeLeft, slopeRight }) => {
  if (x < breakLeft) {
    return slopeLeft * (x - breakLeft);
  };
  if (x > breakRight) {
    return slopeRight * (breakRight - x);
  }
  return 0
};


const antiEdgeAcceleration = (boid) => (
  {
    x: edgeAvoidanceFactor * doubleRamp(
      boid.x,
      {
        breakLeft: edgeMargin,
        slopeLeft: (- edgeSlope),
        breakRight: (app.renderer.screen.width - edgeMargin),
        slopeRight: edgeSlope
      }
    ),
    y: edgeAvoidanceFactor * doubleRamp(
      boid.y,
      {
        breakLeft: edgeMargin,
        slopeLeft: (- edgeSlope),
        breakRight: (app.renderer.screen.height - edgeMargin),
        slopeRight: edgeSlope
      }
    )
  }
);


const alignmentAcceleration = (boid, flockmates) => {
  if (flockmates.length === 0) {
    return {x: 0, y: 0};
  };

  let targetVelx = flockmates.map(b => b.velx).reduce(_.add) / flockmates.length;
  let targetVely = flockmates.map(b => b.vely).reduce(_.add) / flockmates.length;

  return {
    x: (alignmentFactor * (targetVelx - boid.velx)),
    y: (alignmentFactor * (targetVely - boid.vely)),
  };
};


const separationAcceleration = (boid, flockmates) => {
  if (flockmates.length === 0) {
    return {x: 0, y: 0};
  };

  return {
    x: separationFactor * flockmates.map(b => boid.x - b.x).reduce(_.add),
    y: separationFactor * flockmates.map(b => boid.y - b.y).reduce(_.add),
  };
};


const cohesionAcceleration = (boid, flockmates) => {
  if (flockmates.length === 0) {
    return {x: 0, y: 0};
  };

  let targetx = flockmates.map(b => b.x).reduce(_.add) / flockmates.length;
  let targety = flockmates.map(b => b.y).reduce(_.add) / flockmates.length;

  return {
    x: cohesionFactor * (targetx - boid.x),
    y: cohesionFactor * (targety - boid.y),
  }
};


const antiThreatAcceleration = (boid, threat) => {
  if (threat === null) {
    return {x: 0, y: 0};
  };

  let dist = euclidDistance(boid, threat);

  if (dist >= threatAvoidanceRadius) {
    return {x: 0, y: 0};
  };

  return {
    x: - threatAvoidanceFactor / dist * (threat.x - boid.x),
    y: - threatAvoidanceFactor / dist * (threat.y - boid.y),
  }
};


const randomAcceleration = () => (
  {
    x: Math.random() * randomAccelerationFactor,
    y: Math.random() * randomAccelerationFactor,
  }
);


let boids = [];
for (let i = 0; i < boidCount; ++i) {
  let boid = new PIXI.Sprite(boidTexture, {interactiveChildren: false});
  app.stage.addChild(boid);
  boid.x = Math.random() * app.renderer.screen.width;
  boid.y = Math.random() * app.renderer.screen.height;
  boid.velx = Math.random() * 2 - 1;
  boid.vely = Math.random() * 2 - 1;
  // boid.x = 500;
  // boid.y = 100 + i * 20;
  // boid.velx = 1;
  // boid.vely = 0;
  boids.push(boid);

  // const text = new PIXI.Text('text');
  // text.x = boid.x;
  // text.y = boid.y;
  // boid.text = text;
  // app.stage.addChild(text);
};

let state = {
  boids: boids,
  cursor: null,
  threatAge: 0,
  threat: null,
};

const manhattanDistance = (a, b) => (Math.abs(a.x - b.x) + Math.abs(a.y - b.y));
const euclidDistance = (a, b) => (Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y)));


const findFlockmates = (boid, state, radius) => (
  state.boids.filter(
    b => b.x !== boid.x && b.y !== boid.y
  ).filter(
    b => manhattanDistance(boid, b) <= radius
  ).filter(
    b => euclidDistance(boid, b) <= radius
  )
);

cursorZone.on("mousemove", (event) => { state.cursor = event.client; });
cursorZone.on("mouseleave", (event) => { state.cursor = null; });

let elapsed = 0.0;
app.ticker.add((delta) => {
  elapsed += delta;
  state.threatAge += delta;

  if (state.cursor !== null) {
    // cursor.alpha = 1;
    cursor.x = state.cursor.x;
    cursor.y = state.cursor.y;
    // cursor.text.x = cursor.x;
    // cursor.text.y = cursor.y;
    // cursor.text.text = "" + state.cursor;
  } else {
    cursor.alpha = 0;
  };

  if (state.threatAge > 1000) {
    state.threatAge = 0;
    state.threat = {
      x: Math.random() * (app.stage.width - 4 * edgeMargin) + 2 * edgeMargin,
      y: Math.random() * (app.stage.height - 4 * edgeMargin) + 2 * edgeMargin,
    };
    console.log("threat", state.threat);
  };

  boids.forEach(boid => {
    let alignmentFlockmates = findFlockmates(boid, state, alignmentRadius);
    let separationFlockmates = findFlockmates(boid, state, separationRadius);
    let acceleration = _.mergeWith(
      antiEdgeAcceleration(boid),
      alignmentAcceleration(boid, alignmentFlockmates),
      separationAcceleration(boid, separationFlockmates),
      cohesionAcceleration(boid, alignmentFlockmates),
      antiThreatAcceleration(boid, state.cursor),
      antiThreatAcceleration(boid, state.threat),
      randomAcceleration(),
      _.add,
    );

    boid.velx += acceleration.x;
    boid.vely += acceleration.y;

    let speed = Math.sqrt(boid.velx * boid.velx + boid.vely * boid.vely);

    if (speed < minSpeed) {
      boid.velx = boid.velx / speed * minSpeed;
      boid.vely = boid.vely / speed * minSpeed;
    }

    if (speed > maxSpeed) {
      boid.velx = boid.velx / speed * maxSpeed;
      boid.vely = boid.vely / speed * maxSpeed;
    }

    boid.x = (boid.x + delta * boid.velx);
    boid.y = (boid.y + delta * boid.vely);

    // boid.text.text = "" + boid.velx;
    // boid.text.x = boid.x;
    // boid.text.y = boid.y;

    // boid.x = (boid.x + delta * boid.velx) % app.renderer.screen.width;
    // boid.y = (boid.y + delta * boid.vely) % app.renderer.screen.height;

    boid.rotation = Math.atan2(boid.vely, boid.velx);
  })

});
